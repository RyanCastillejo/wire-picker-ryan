﻿//declaration of variables
var result1 = "<div class=\"wire-picker-modal wire-picker-modal-success\"><h1 class=\"header-h2-center\">Great! Your system works with these thermostats.</h1><div class=\"products-block\"><ul><li><img src=\"~/Images/compatibility-wi-fi.jpg\" alt=\"Wi-Fi Thermostat\" /><h2 class=\"list-title\">Wi-Fi Thermostat</h2><p>Comfort control from your phone.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>129.99</p><a href=\"https://sensicomfort.com/products/wi-fi-thermostat\" class=\"primary-button\">Learn more</a></li><li><img src=\"~/Images/compatibility-touch.jpg\" alt=\"Touch Wi-Fi Thermostat\" /><h2 class=\"list-title\">Touch Wi-Fi Thermostat</h2><p>Comfort control in color.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>199.99</p><a href=\"https://sensicomfort.com/products/touch-thermostat\" class=\"primary-button\">Learn more</a></li></ul></div></div>",
    result2 = "<div class=\"wire-picker-modal wire-picker-modal-success\"><h1 class=\"header-h2-center\">Great! Your system works with these thermostats.</h1><div class=\"products-block\"><ul><li><img src=\"~/Images/compatibility-wi-fi.jpg\" alt=\"Wi-Fi Thermostat\" /><h2 class=\"list-title\">Wi-Fi Thermostat</h2><p>Comfort control from your phone.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>129.99</p><a href=\"https://sensicomfort.com/products/wi-fi-thermostat\" class=\"primary-button\">Learn more</a></li><li><img src=\"~/Images/compatibility-touch.jpg\" alt=\"Touch Wi-Fi Thermostat\" /><h2 class=\"list-title\">Touch Wi-Fi Thermostat</h2><p>Comfort control in color.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>199.99</p><a href=\"https://sensicomfort.com/products/touch-thermostat\" class=\"primary-button\">Learn more</a></li></ul></div></div>",
    result3 = "<div class=\"wire-picker-modal wire-picker-modal-simple\"><h1 class=\"header-h2-center\">Sorry, your system in not compatible.</h1><p class=\"intro-text\">Our current Sensi thermostats will not work with your system. We are working hard to include your type of system in future thermostat models.</p></div>",
    result4 = "<div class=\"wire-picker-modal\"><h1 class=\"header-h2-center\">Let us help you figure it out.</h1><p>We may need to take a closer look to see if your system is compatible. You can:</p><p>1) Take a picture of your wiring after removing the thermostat cover.<br />2) Contact our support team by email or phone.</p></div>",
    result5 = "<div class=\"wire-picker-modal wire-picker-modal-simple\"><h1 class=\"header-h2-center\">Hmm, something isn&rsquo;t quite right.</h1><p class=\"intro-text\">Let&rsquo;s go back and make sure that you&rsquo;ve entered all the connected wires. There should be an R, RH, or RC wire attached to your thermostat.</p></div>",
    result6 = "<div class=\"wire-picker-modal wire-picker-modal-success\"><h1 class=\"header-h2-center\">Your system is compatible but may need a common wire.</h1><p class=\"intro-text\">Based on your thermostat wiring, Sensi Wi-Fi Thermostat is compatible with no modifications. Your system will need a common wire to be compatible with Sensi Touch Wi-Fi Thermostat. <a href=\"https://sensicomfort.com/c-wire\">Learn more about adding a common wire.</a></p><div class=\"products-block\"><ul><li><img src=\"~/Images/compatibility-wi-fi.jpg\" alt=\"Wi-Fi Thermostat\" /><h2 class=\"list-title\">Wi-Fi Thermostat</h2><p>Comfort control from your phone.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>129.99</p><a href=\"https://sensicomfort.com/products/wi-fi-thermostat\" class=\"primary-button\">Learn more</a></li><li><img src=\"~/Images/compatibility-touch.jpg\" alt=\"Touch Wi-Fi Thermostat\" /><h2 class=\"list-title\">Touch Wi-Fi Thermostat</h2><p>Comfort control in color.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>199.99</p><a href=\"https://sensicomfort.com/products/touch-thermostat\" class=\"primary-button\">Learn more</a></li></ul></div></div>",
    result7 = "<div class=\"wire-picker-modal wire-picker-modal-success\"><h1 class=\"header-h2-center\">Your system is compatible but needs a common wire.</h1><p class=\"intro-text\">Based on your thermostat wiring, your system will need a common wire to be compatible with Sensi Wi-Fi Thermostat and Sensi Touch Wi-Fi Thermostat. <a href=\"https://sensicomfort.com/c-wire\">Learn more about adding a common wire</a>.</p><div class=\"products-block\"><ul><li><img src=\"~/Images/compatibility-wi-fi.jpg\" alt=\"Wi-Fi Thermostat\" /><h2 class=\"list-title\">Wi-Fi Thermostat</h2><p>Comfort control from your phone.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>129.99</p><a href=\"https://sensicomfort.com/products/wi-fi-thermostat\" class=\"primary-button\">Learn more</a></li><li><img src=\"~/Images/compatibility-touch.jpg\" alt=\"Touch Wi-Fi Thermostat\" /><h2 class=\"list-title\">Touch Wi-Fi Thermostat</h2><p>Comfort control in color.</p><p class=\"products-section-price\"><span class=\"unit\">$</span>199.99</p><a href=\"https://sensicomfort.com/products/touch-thermostat\" class=\"primary-button\">Learn more</a></li></ul></div></div>";

var result = 0,
    power = false,
    heating = false,
    pump = false,
    fan = false,
    common = false,
    changeover = false,
    malf = false,
    more = false,
    other = false;

var buttonR = 0,
    buttonW = 0,
    buttonY = 0,
    buttonG = 0,
    buttonRH = 0,
    buttonW1 = 0,
    buttonY1 = 0,
    buttonO = 0,
    buttonRC = 0,
    buttonWE = 0,
    buttonY2 = 0,
    buttonB = 0,
    buttonC = 0,
    buttonW2 = 0,
    buttonL = 0,
    buttonOB = 0,
    buttonX = 0,
    buttonE = 0,
    buttonAUX = 0,
    buttonA1 = 0,
    buttonS = 0,
    buttonS1 = 0,
    buttonS2 = 0,
    buttonH = 0,
    buttonH2 = 0,
    buttonHUM = 0,
    buttonHUM1 = 0,
    buttonHUM2 = 0,
    buttonD = 0,
    buttonDH = 0,
    buttonDHUM = 0,
    buttonDEHUM = 0,
    buttonD2 = 0,
    buttonDH2 = 0,
    buttonDHUM2 = 0,
    buttonABC = 0,
    button12 = 0,
    button123 = 0,
    buttonStar = 0,
    buttonOther = 0;


//after load page
$(window).ready(function () {

    //Show more button
    $("#wire-picker-show-more-button").click(function () {
        $('#divMoreItems').slideDown();
        $("#wire-picker-show-more-button").hide();
    });

    //Check compatibility button
    $("#wire-picker-form-submit-button").click(function () {
        resetVariables();
        checkCheckbox();
        checkConditions();
        checkConditions2();
        checkConditions3();
        $("#modalId").modal("show");
    });

    //Button R and RH = disable RC
    $("#button-R-field, #button-RH-field").on("change", function () {
        checkCheckbox();
        if (buttonR > 0 && buttonRH > 0) {
            funcDisableCheckbox("button-RC-field");
        }
        else {
            funcEnableCheckbox("button-RC-field");
        }
    });

    //Button R and RC = disable RH
    $("#button-R-field, #button-RC-field").on("change", function () {
        checkCheckbox();
        if (buttonR > 0 && buttonRC > 0) {
            funcDisableCheckbox("button-RH-field");
        }
        else {
            funcEnableCheckbox("button-RH-field");
        }
    });

    //Button RH and RC = disable R
    $("#button-RH-field, #button-RC-field").on("change", function () {
        checkCheckbox();
        if (buttonRH > 0 && buttonRC > 0) {
            funcDisableCheckbox("button-R-field");
        }
        else {
            funcEnableCheckbox("button-R-field");
        }
    });

    //Button W = disable W1, WE, E, AUX
    $("#button-W-field").on("change", function () {
        checkCheckbox();
        if (buttonW > 0) {
            funcDisableCheckbox("button-W1-field");
            funcDisableCheckbox("button-WE-field");
            funcDisableCheckbox("button-E-field");
            funcDisableCheckbox("button-AUX-field");
        }
        else {
            funcEnableCheckbox("button-W1-field");
            funcEnableCheckbox("button-WE-field");
            funcEnableCheckbox("button-E-field");
            funcEnableCheckbox("button-AUX-field");
        }
    });

    //Button WE = disable W, W1, E, AUX
    $("#button-WE-field").on("change", function () {
        checkCheckbox();
        if (buttonWE > 0) {
            funcDisableCheckbox("button-W-field");
            funcDisableCheckbox("button-W1-field");
            funcDisableCheckbox("button-E-field");
            funcDisableCheckbox("button-AUX-field");
        }
        else {
            funcEnableCheckbox("button-W-field");
            funcEnableCheckbox("button-W1-field");
            funcEnableCheckbox("button-E-field");
            funcEnableCheckbox("button-AUX-field");
        }
    });

    //Button AUX = disable W, W1, WE
    $("#button-AUX-field").on("change", function () {
        checkCheckbox();
        if (buttonAUX > 0) {
            funcDisableCheckbox("button-W-field");
            funcDisableCheckbox("button-W1-field");
            funcDisableCheckbox("button-WE-field");
        }
        else {
            funcEnableCheckbox("button-W-field");
            funcEnableCheckbox("button-W1-field");
            funcEnableCheckbox("button-WE-field");
            funcEnableCheckbox("button-E-field");
        }
    });

    //Button W1 = disable W, WE, E, AUX
    $("#button-W1-field").on("change", function () {
        checkCheckbox();
        if (buttonW1 > 0) {
            funcDisableCheckbox("button-W-field");
            funcDisableCheckbox("button-WE-field");
            funcDisableCheckbox("button-E-field");
            funcDisableCheckbox("button-AUX-field");
        }
        else {
            funcEnableCheckbox("button-W-field");
            funcEnableCheckbox("button-WE-field");
            funcEnableCheckbox("button-E-field");
            funcEnableCheckbox("button-AUX-field");
        }
    });

    //Button E = disable W, WE,AUX
    $("#button-E-field").on("change", function () {
        checkCheckbox();
        if (buttonE > 0) {
            funcDisableCheckbox("button-W-field");
            funcDisableCheckbox("button-WE-field");
            funcDisableCheckbox("button-AUX-field");
        }
        else {
            funcEnableCheckbox("button-W-field");
            funcEnableCheckbox("button-W1-field");
            funcEnableCheckbox("button-WE-field");
            funcEnableCheckbox("button-AUX-field");
        }
    });

    //Button W1 and W2 = disable W, WE, AUX
    $("#button-W1-field, #button-W2-field").on("change", function () {
        //enable E
        checkCheckbox();
        if (buttonW1 > 0 && buttonW2 > 0) {
            funcDisableCheckbox("button-W-field");
            funcDisableCheckbox("button-WE-field");
            funcEnableCheckbox("button-E-field");
            funcDisableCheckbox("button-AUX-field");
        }

        //disable E
        if (buttonW1 > 0 && !(buttonW2 > 0)) {
            funcDisableCheckbox("button-E-field");
        }
    });

    //Button Y = disable Y1
    $("#button-Y-field").on("change", function () {
        checkCheckbox();
        if (buttonY > 0) {
            funcDisableCheckbox("button-Y1-field");
        }
        else {
            funcEnableCheckbox("button-Y1-field");
        }
    });

    //Button Y1 = disable Y
    $("#button-Y1-field").on("change", function () {
        checkCheckbox();
        if (buttonY1 > 0) {
            funcDisableCheckbox("button-Y-field");
        }
        else {
            funcEnableCheckbox("button-Y-field");
        }
    });

    //Button O or B = disable OB
    $("#button-O-field, #button-B-field").on("change", function () {
        //disable OB
        checkCheckbox();
        if (buttonO > 0 || buttonB > 0) {
            funcDisableCheckbox("button-OB-field");
        }

        //enable OB
        else {
            funcEnableCheckbox("button-OB-field");
        }

        //disable C, X, OB
        if (buttonO > 0 && buttonB > 0) {
            funcDisableCheckbox("button-C-field");
            funcDisableCheckbox("button-X-field");
            funcDisableCheckbox("button-OB-field");
        }
        
        //enable C, X
        else {
            funcEnableCheckbox("button-C-field");
            funcEnableCheckbox("button-X-field");
        }
    });

    //Button OB = disable O, B
    $("#button-OB-field").on("change", function () {
        checkCheckbox();
        if (buttonOB > 0) {
            funcDisableCheckbox("button-O-field");
            funcDisableCheckbox("button-B-field");
        }
        else {
            funcEnableCheckbox("button-O-field");
            funcEnableCheckbox("button-B-field");
        }
    });

    //Button C = disable X
    $("#button-C-field").on("change", function () {
        checkCheckbox();
        if (buttonC > 0) {
            funcDisableCheckbox("button-X-field");
        }
        else {
            funcEnableCheckbox("button-X-field");
        }
    });

    //Button X = disable C
    $("#button-X-field").on("change", function () {
        checkCheckbox();
        if (buttonX > 0) {
            funcDisableCheckbox("button-C-field");
        }
        else {
            funcEnableCheckbox("button-C-field");
        }
    });
    
});

//disable checkbox
function funcDisableCheckbox(Id) {
    $("#"+Id).attr("disabled", true);
}

//enable checkbox
function funcEnableCheckbox(Id) {
    $("#" + Id).attr("disabled", false);
}

function checkCheckbox(){
    buttonR = $("#button-R-field:checkbox:checked").length;
    buttonW = $("#button-W-field:checkbox:checked").length;
    buttonY = $("#button-Y-field:checkbox:checked").length;
    buttonG = $("#button-G-field:checkbox:checked").length;
    buttonRH = $("#button-RH-field:checkbox:checked").length;
    buttonW1 = $("#button-W1-field:checkbox:checked").length;
    buttonY1 = $("#button-Y1-field:checkbox:checked").length;
    buttonO = $("#button-O-field:checkbox:checked").length;
    buttonRC = $("#button-RC-field:checkbox:checked").length;
    buttonWE = $("#button-WE-field:checkbox:checked").length;
    buttonY2 = $("#button-Y2-field:checkbox:checked").length;
    buttonB = $("#button-B-field:checkbox:checked").length;
    buttonC = $("#button-C-field:checkbox:checked").length;
    buttonW2 = $("#button-W2-field:checkbox:checked").length;
    buttonL = $("#button-L-field:checkbox:checked").length;
    buttonOB = $("#button-OB-field:checkbox:checked").length;
    buttonX = $("#button-X-field:checkbox:checked").length;
    buttonE = $("#button-E-field:checkbox:checked").length;
    buttonAUX = $("#button-AUX-field:checkbox:checked").length;
    buttonA1 = $("#button-A1-field:checkbox:checked").length;
    buttonS = $("#button-S-field:checkbox:checked").length;
    buttonS1 = $("#button-S1-field:checkbox:checked").length;
    buttonS2 = $("#button-S2-field:checkbox:checked").length;
    buttonH = $("#button-H-field:checkbox:checked").length;
    buttonH2 = $("#button-H2-field:checkbox:checked").length;
    buttonHUM = $("#button-HUM-field:checkbox:checked").length;
    buttonHUM1 = $("#button-HUM1-field:checkbox:checked").length;
    buttonHUM2 = $("#button-HUM2-field:checkbox:checked").length;
    buttonD = $("#button-D-field:checkbox:checked").length;
    buttonDH = $("#button-DH-field:checkbox:checked").length;
    buttonDHUM = $("#button-DHUM-field:checkbox:checked").length;
    buttonDEHUM = $("#button-DEHUM-field:checkbox:checked").length;
    buttonD2 = $("#button-D2-field:checkbox:checked").length;
    buttonDH2 = $("#button-DH2-field:checkbox:checked").length;
    buttonDHUM2 = $("#button-DHUM2-field:checkbox:checked").length;
    buttonABC = $("#button-ABC-field:checkbox:checked").length;
    button12 = $("#button-12-field:checkbox:checked").length;
    button123 = $("#button-123-field:checkbox:checked").length;
    buttonStar = $("#button-star-field:checkbox:checked").length;
    buttonOther = $("#button-other-field:checkbox:checked").length;
}

function checkConditions() {
    
    if (buttonR || buttonRH || buttonRC) {
        power = true;
    }
    if (buttonW || buttonW1 || buttonWE || buttonW2 || buttonE || buttonAUX) {
        heating = true;
    }
    if (buttonY || buttonY1 || buttonY2) {
        pump = true;
    }
    if (buttonG) {
        fan = true;
    }
    if (buttonC || buttonX) {
        common = true;
    }
    if (buttonO || buttonB || buttonOB) {
        changeover = true;
    }
    if (buttonL) {
        malf = true;
    }
    if (buttonA1 || buttonS || buttonS1 || buttonS2 || buttonH || buttonH2 || buttonHUM || buttonHUM1 || buttonHUM2 || buttonD 
      || buttonDH || buttonDHUM || buttonDEHUM || buttonD2 || buttonDH2 || buttonDHUM2 || buttonABC || button12 || button123 || buttonStar) {
        more = true;
    }
    if (buttonOther) {
        other = true;
    }
}

function checkConditions2() {

    if (power == true) {
        result = 5;

        if (heating == false && pump == false && fan == true && common == false && changeover == false) {
            result = 3;
        }
        if (heating == true && pump == false && common == true && changeover == false) {
            result = 1;
        }
        if (heating == false && pump == true && common == true && changeover == false) {
            result = 1;
        }
        if (heating == false && pump == false && fan == true && common == true && changeover == false) {
            result = 1;
        }
        if (heating == true && pump == true && changeover == false) {
            if (common == true) {
                result = 1;
            }
            else {
                result = 6;
            }
        }
        if (heating == false && pump == true && common == false && changeover == true) {
            result = 3;
        }
        if (heating == false && pump == true && common == true && changeover == true) {
            result = 1;
        }
        if (heating == true && pump == true && changeover == true) {
            result = 2;
        }
        if (heating == false && pump == false && fan == false) {
            result = 3;
        }
        if (pump == true && fan == true && changeover == true && common == false) {
            result = 6;
        }
        if (pump == true && fan == true && changeover == true && common == false) {
            result = 6;
        }
        if (heating == true && fan == true && pump == false && common == false) {
            result = 6;
        }
        if (heating == false && pump == true && fan == true && common == false) {
            result = 6;
        }
        if (heating == true && pump == false && fan == false && common == false) {
            result = 6;
        }
        if (heating == false && pump == true && fan == false && common == false) {
            result = 6;
        }
        if (power == true && heating == true && pump == false && fan == false && common == false) {
            result = 7;
        }
        if (power == true && heating == true && pump == false && fan == true && common == false) {
            result = 7;
        }
        if (power == true && heating == false && pump == true && fan == true && common == false) {
            result = 7;
        }
        if (power == true && pump == true && fan == true && changeover == true && common == false) {
            result = 7;
        }
        if (power == true && heating == true && pump == true && fan == true && changeover == true && common == false) {
            result = 7;
        }
    }
    else {
        result = 5;
    }

    if (more == true) {
        result = 3;
    }

    if (other == true) {
        result = 4;
    }

}

function checkConditions3() {

    if (result > 0) {
        if (result == 1) {
            $("#container").html(result1);
        }

        else if (result == 2) {
            $("#container").html(result2);
        }

        else if (result == 3) {
            $("#container").html(result3);
        }

        else if (result == 4) {
            $("#container").html(result4);
        }

        else if (result == 5) {
            $("#container").html(result5);
        }

        else if (result == 6) {
            $("#container").html(result6);
        }

        else if (result == 7) {
            $("#container").html(result7);
        }
    }
}

function resetVariables() {
    result = 0;
    power = false;
    heating = false;
    pump = false;
    fan = false;
    common = false;
    changeover = false;
    malf = false;
    more = false;
    other = false;

    buttonR = 0;
    buttonW = 0;
    buttonY = 0;
    buttonG = 0;
    buttonRH = 0;
    buttonW1 = 0;
    buttonY1 = 0;
    buttonO = 0;
    buttonRC = 0;
    buttonWE = 0;
    buttonY2 = 0;
    buttonB = 0;
    buttonC = 0;
    buttonW2 = 0;
    buttonL = 0;
    buttonOB = 0;
    buttonX = 0;
    buttonE = 0;
    buttonAUX = 0;
    buttonA1 = 0;
    buttonS = 0;
    buttonS1 = 0;
    buttonS2 = 0;
    buttonH = 0;
    buttonH2 = 0;
    buttonHUM = 0;
    buttonHUM1 = 0;
    buttonHUM2 = 0;
    buttonD = 0;
    buttonDH = 0;
    buttonDHUM = 0;
    buttonDEHUM = 0;
    buttonD2 = 0;
    buttonDH2 = 0;
    buttonDHUM2 = 0;
    buttonABC = 0;
    button12 = 0;
    button123 = 0;
    buttonStar = 0;
    buttonOther = 0;
}